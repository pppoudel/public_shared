# Simple Container Deployment Automation Script

## What is this  
This project contains set of scripts and ideas to make automate your container deployment process and also make it portable
among different container hosting services - whether it's local docker deployment or hosted on IBM Bluemix® etc.
It shows basic scripting ideas, so that the same docker-compose.yml and other related files can be used independent of service provider.
For more details on how to use it, see my blog post [Making Your Container Deployment Portable] (https://purnapoudel.blogspot.com/2017/02/making-your-container-deployment.html)