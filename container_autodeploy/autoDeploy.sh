#!/bin/sh
# Author: Purna Poudel
# Created on: 23 February, 2017

# project directory
pDir='.'
#property file
propFile=depl.properties

function usage {
	printf "Usage: $0 <options>\n";
	printf "Options:\n";
	printf "%10s-t|--conttype:-u|--username:-p|--password\n";
}

OPTS=$(getopt -o t:u:p: -l conttype:,username:,password:, -- "$0" "$@");
if [ $? != 0 ]; then
	echo "Unrecognised command line option encountered!";
	usage;
	exit 1;
fi
eval set -- "$OPTS";

while true; do
	case "$1" in
		-t|--conttype)
			conttypearg="$1";
			conttype=$2;
			shift 2;;
		-u|--username)
			usernamearg="$1";
			username=$2;
			shift 2;;
		-p|--password)
			passwordarg="$1";
			password=$2;
			shift 2;;
		*)
			shift;
			break;;
	esac
done
if [[ $conttype == "" ]]; then
	echo "Valid non empty value for '--conttype' or '-t' is required."
	usage;
	exit 1
fi

# Reads each line if 'prefix' matches the supplied value of $conttype. Also excludes all commented (starting with #) lines, all empty lines and not related properties.
for _line in `cat "${pDir}/${propFile}" |  egrep '(^'$conttype'|^all)' |grep -v -e'#' | grep -v -e'^$'`; do
    echo "Reading line: $_line from source file: ${pDir}/${propFile}";
    # Assign property name to variable '_key'
		# Also remove the prefix, which is supposed to be the identifier for particular environment in depl.properties file.
		# the final 'xargs' removes the leading and trailing blank spaces.
		_key=$(echo $_line | awk 'BEGIN {FS="="}{print $1}' | awk 'BEGIN {FS=OFS="_"}{print substr($0, index($0,$2))}' | xargs);
		# Assign property value to variable '_value'
		_value=`eval echo $_line | cut -d '=' -f2`;
		# Also declare shell variable and export to use as environment variable,
		declare $_key=$(echo $_value | xargs);
		echo "Setting environment variable: ${_key}=${!_key}";
		export ${_key}=${!_key};
done

if [[ $conttype == "bluemix" ]]; then
	# First log into CloudFoundry
	# cf login [-a API_URL] [-u USERNAME] [-p PASSWORD] [-o ORG] [-s SPACE]
	cf login -a ${API_ENDPOINT} -u ${username} -p ${password} -o ${ORG_NAME} -s ${SPACE_NAME};
	retSts=$?;
	if [ $retSts -ne 0 ]; then
		echo "Login to CloudFoundry failed with return code: "$retSts;
		exit $retSts;
	fi
	# then log into the IBM Container
	cf ic login
	retSts=$?;
	if [ $retSts -ne 0 ]; then	
		echo "Login to IBM Container failed with return code: $retSts;"
		exit $retSts;
	fi
fi
# Stop and remove if container are running.
docker-compose ps | grep "Up";
retSts=$?;
if [ $retSts -eq 0 ]; then
	echo "Stopping existing docker-compose container...";
	docker-compose stop;
	sleep 5;
fi
docker-compose ps -q | grep "[a-z0-9]"
retSts=$?;
if [ $retSts -eq 0 ]; then
	echo "Removing existing docker-compose container...";
	docker-compose rm -f;
	sleep 5;
fi
# execute docker-compose
docker-compose up -d;
sleep 20;
# Make sure container built and running
docker-compose ps;
