# This Demo application shows simple encryption/decryption usage of com.ibm.websphere.crypto.PasswordUtil class
See the details in my blog [How to use WLP passwordUtilities for encryption/decryption] (https://purnapoudel.blogspot.com/2017/10/how-to-use-wlp-passwordutilities-for.html)

### How to use it?  
Follow the steps below:  


1. Replace your server.xml with the one included in this project. 
   Depending upon the version of your WebSphere Liberty Profile (WLP) server, uncomment or comment line(s) as
	 instructed in the server.xml
	 
2. Copy my_enc_key.xml into the correct directory as in server.xml

3. Deploy provided application 'pwdencdemo.war'. Make sure application location is correct as specified in the server.xml

4. [optional] update passutil.jsp.

4. Access http://\<host\>:\<port\>/pwdencdemo/passutil.jsp

