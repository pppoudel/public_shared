# How to update ibm-web-ext.xml, and/or ibm-web-ext.xmi inside EAR/WAR recursively

## Purpose:
Use this script to update EAR/WAR file(s) to enable the file serving and add extendedDocumentRoot feature. 
Script updates ibm-web-ext.xml, and/or ibm-web-ext.xmi file(s) in all Web modules inside each EAR file.  
Read my blog [How to Make your WAS Static Content Deployment Shareable] (https://purnapoudel.blogspot.com/2017/09/extendedDocumentRoot.html) for more details on this topic.  

## Setup:  
Script is written using Apache Ant framework. So, it requires Apache Ant and few additional libraries found under ${basedir}/lib  
1) ant-contrib-1.0b4.jar - Refer to [AntContrib] (https://sourceforge.net/projects/ant-contrib/files/ant-contrib/1.0b3/)  
2) xmltask-1.16c v1.16.jar - Refer to [XMLTask] (http://www.oopsconsultancy.com/software/xmltask/)  

### Property file: pkgupdate.properties  
Open it and update the value of following properties as necessary:  

#### installableapps.loc  
It is the location where you put all your EAR/WAR files to be updated.   
installableapps.loc=C:/IBM/installableApps  

#### failOnError  
If failOnError is true, it stops processing further even one update fails. If you want to continue the processing assign 'false' value.  
For example:  
failOnError=false  

#### ext.doc.root, and append.ctxroot  
It is a runtime property. The value will be used to formulate the value for extendedDocumentRoot. Here is how the value is formed.  
if 'append.ctxroot=true'; then  
extendedDocumentRoot=${ext.doc.root}+${root-context}  
Note: root-context value is extracted by script from application.xml    
For example:  
ext.doc.root=/opt/ibm/static_contents  
and root-context=/myapp  
then  
extendedDocumentRoot=/opt/ibm/static_contents/myapp  

if 'append.ctxroot=false'; then  
extendedDocumentRoot=${ext.doc.root}  
For example:  
ext.doc.root=/opt/ibm/static_contents  

## How to run:  
- Set JAVA_HOME and ANT_HOME appropriately in extDocRootUpdater.sh or extDocRootUpdater.cmd then  
- launch command prompt (windows) or Shell (Unix/Linux)  
- cd to extdocroot directory  
- execute extDocRootUpdater.sh or extDocRootUpdater.cmd  